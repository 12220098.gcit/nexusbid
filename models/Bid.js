const mongoose = require('mongoose');

const bidSchema = new mongoose.Schema({
  productId: { type: String, required: true },
  amount: { type: Number, required: true },
  bidder: { type: String, required: true },
});

const Bid = mongoose.model('Bid', bidSchema);

module.exports = Bid;