const mongoose = require('mongoose')
// const validator = require('validator')

const productSchema = new mongoose.Schema({
    productName: {
        type: String,
        required: [true, 'A name should be unique'],
    },
    description: {
        type: String,
        required: true,
        // trim: true,
        // maxLength: [
        //     30,
        //     'A news description must have less or equal to 40 characters',
        // ],
        // minLength: [
        //     10,
        //     'A news description must have more or equal than 10 characters',
        // ],
    },
    StartingPrice: {
        type: String,
        required: [true, 'A name should be unique'],
    },
    hours: {
        type: String,
        required: [true, 'A name should be unique'],
    },
    minutes:{
        type :String,
        required: [true, 'A name should be unique'],
    },
    categery:{
        type:String,
        required: [true, 'A name should be unique'],
    },
   
  
    imageCover: {
        type: String,
        // required: [true, 'A tour must have a cover image'],
    },
   
    // SME: Subject Matter Experts
    
})

// productSchema.pre(/^find/, function(next){
//     this.populate({
//         path: 'user',
//         select: 'name'
//     })
//     next()
// })

const products = mongoose.model('Products', productSchema)
module.exports = products
