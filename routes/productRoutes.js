const express = require('express')
const productController = require('./../controller/productController')
// const authController = require('./../controllers/authController')
const router = express.Router()

router 
    .route('/')
    .get(productController.getAllNews)
    .post(productController.createNews)

router
    .route('/:id')
    .get(productController.getNews)
    .patch(productController.updateNews)
    .delete(productController.deleteNews)

router

module.exports = router
