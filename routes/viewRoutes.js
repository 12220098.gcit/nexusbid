const express = require('express')
const router = express.Router()
const viewsController = require('./../controller/viewController')
const authController = require('./../controller/authController')

router.get('/', viewsController.getHome)
router.get('/home',authController.protect, viewsController.getHome2)
router.get('/login', viewsController.getLoginForm)
router.get('/signup', viewsController.getSignupForm)
router.get('/me',authController.protect,viewsController.getProfile)
router.get('/admin',authController.protect, viewsController.getAdmin)
router.get('/product',authController.protect, viewsController.getProducts)


module.exports = router
