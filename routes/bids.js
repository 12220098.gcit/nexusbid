const express = require('express');
const router = express.Router();
const Bid = require('../models/Bid');
const bidController = require('../controller/bidController');


// Create a new bid
router.post('/', async (req, res) => {
  try {
    const { productId, amount, bidder } = req.body;

    const bid = new Bid({
      productId,
      amount,
      bidder,
    });

    await bid.save();

    res.status(201).json(bid);
  } catch (err) {
    res.status(500).json({ error: 'Failed to create a bid' });
  }
});

// Get all bids for a product
router.get('/:productId', async (req, res) => {
  try {
    const { productId } = req.params;

    const bids = await Bid.find({ productId });

    res.json(bids);
  } catch (err) {
    res.status(500).json({ error: 'Failed to get bids' });
  }
});

module.exports = router;


// Create a new bid
router.post('/', bidController.createBid);

// Get all bids for a product
router.get('/:productId', bidController.getBidsByProduct);

module.exports = router;
