// import { showAlert } from './alert.js'



export const addProduct = async (productName,description,StartingPrice,hours,minutes,categery) => {
    try {
       
        const res = await axios({
            method: 'POST',
            url: 'https://nexusbid.onrender.com/api/v1/product',
            data: {
                productName,
                description,
                StartingPrice,
                hours,
                minutes,
                categery
            },
        })
        console.log(res)
        if (res.data.status === 'success') {
            console.log('success', 'product created successfully!')
            window.setTimeout(() => {
                location.assign('/admin')
            }, 1500)
        }
    } catch (err) {
        let message = typeof err.response !== 'undefined'
            ? err.response.data.message
            : err.message
        console.log('error', 'something went wrong', message)
    }
}



document.querySelector('.form').addEventListener('click', (e) => {
    e.preventDefault()
    const productName = document.getElementById('product-name').value
    const description = document.getElementById('product-description').value
    const StartingPrice = document.getElementById('starting-price').value
    const hours = document.getElementById('bidding-duration-hours').value
    const minutes = document.getElementById('bidding-duration-minutes').value
    const categery = document.getElementById('category').value
    addProduct(productName, description, StartingPrice,hours,minutes,categery)
})