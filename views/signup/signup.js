const form = document.querySelector('form');
const usernameInput = document.querySelector('#username');
const emailInput = document.querySelector('#email');
const passwordInput = document.querySelector('#password');
const confirmPasswordInput = document.querySelector('#confirm-password');

form.addEventListener('submit', (e) => {
  e.preventDefault();
  
  // Perform validation on form inputs
  let isValid = true;
  
  // Validate username
  if (usernameInput.value.trim() === '') {
    setErrorFor(usernameInput, 'Username is required');
    isValid = false;
  } else {
    setSuccessFor(usernameInput);
  }
  
  // Validate email
  if (emailInput.value.trim() === '') {
    setErrorFor(emailInput, 'Email is required');
    isValid = false;
  } else if (!isValidEmail(emailInput.value)) {
    setErrorFor(emailInput, 'Email is not valid');
    isValid = false;
  } else {
    setSuccessFor(emailInput);
  }
  
  // Validate password
  if (passwordInput.value.trim() === '') {
    setErrorFor(passwordInput, 'Password is required');
    isValid = false;
  } else if (passwordInput.value.length < 6) {
    setErrorFor(passwordInput, 'Password must be at least 6 characters');
    isValid = false;
  } else {
    setSuccessFor(passwordInput);
  }
  
  // Validate confirm password
  if (confirmPasswordInput.value.trim() === '') {
    setErrorFor(confirmPasswordInput, 'Please confirm your password');
    isValid = false;
  } else if (confirmPasswordInput.value !== passwordInput.value) {
    setErrorFor(confirmPasswordInput, 'Passwords do not match');
    isValid = false;
  } else {
    setSuccessFor(confirmPasswordInput);
  }
  
  // If form inputs are valid, submit the form
  if (isValid) {
    form.submit();
  }
});

function setErrorFor(input, message) {
  const formControl = input.parentElement;
  const errorMessage = formControl.querySelector('.error-message');
  
  formControl.classList.add('error');
  errorMessage.innerText = message;
}

function setSuccessFor(input) {
  const formControl = input.parentElement;
  
  formControl.classList.remove('error');
}

function isValidEmail(email) {
  return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email);
}

const phoneNumberInput = document.getElementById("phone-number");

// Add event listener for input changes
phoneNumberInput.addEventListener("input", () => {
  // Remove any non-numeric characters from the input value
  let phoneNumber = phoneNumberInput.value.replace(/\D/g, "");
  
  // Add the country code prefix if it's not already present
  if (!phoneNumber.startsWith("+975")) {
    phoneNumber = "+975" + phoneNumber;
  }
  
  // Limit the input to 8 numeric characters
  phoneNumber = phoneNumber.slice(0, 5) + phoneNumber.slice(5, 13).replace(/\D/g, "");
  
  // Check if the phone number starts with 17 or 77
  if (!phoneNumber.startsWith("+97517") && !phoneNumber.startsWith("+97577")) {
    // Display an error message
    document.getElementById("phone-number-error").textContent = "Phone number must start with +97517 or +97577";
    // Disable the sign up button
    document.getElementById("sign-up-button").disabled = true;
  } else {
    // Clear the error message
    document.getElementById("phone-number-error").textContent = "";
    // Enable the sign up button
    document.getElementById("sign-up-button").disabled = false;
  }
  
  // Set the formatted phone number back to the input value
  phoneNumberInput.value = phoneNumber;
});

