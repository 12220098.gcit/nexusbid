import { showAlert } from './alert.js'
// console.log("SIGNUP TEST")
export const signup = async (name, email, password, passwordConfirm) => {
    try {
        console.log("CALLING")
        const res = await axios({
            method: 'POST',
            url: 'https://nexusbid.onrender.com/api/v1/users/signup',
            data: {
                name,
                email,
                password,
                passwordConfirm,
            },
        })
        console.log(res)
        if (res.data.status === 'success') {
            showAlert('success', 'Account created successfully!')
            window.setTimeout(() => {
                location.assign('/')
            }, 1500)
        }
    } catch (err) {
        let message = typeof err.response !== 'undefined'
            ? err.response.data.message
            : err.message
        showAlert('error', 'Error: Passwords are not the same!', message)
    }
}



document.querySelector('#signup-btn').addEventListener('click', (e) => {
    console.log("here")
    e.preventDefault()
    const name = document.getElementById('name').value
    const email = document.getElementById('email').value
    const password = document.getElementById('password').value
    const passwordConfirm = document.getElementById('passwordConfirm').value
    signup(name, email, password, passwordConfirm)
})