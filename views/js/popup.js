function showPopup() {
    document.getElementById("bidPopup").style.display = "flex";
    document.getElementById("bidPopup").style.display = "";

  }

  function closePopup() {
    document.getElementById("bidPopup").style.display = "none";
  }

  function incrementBid() {
    var bidAmount = parseInt(document.getElementById("bidAmount").value);
    document.getElementById("bidAmount").value = bidAmount + 1;
  }

  function decrementBid() {
    var bidAmount = parseInt(document.getElementById("bidAmount").value);
    if (bidAmount > 0) {
      document.getElementById("bidAmount").value = bidAmount - 1;
    }
  }

  function placeBid() {
    var bidAmount = parseInt(document.getElementById("bidAmount").value);
    document.getElementById("currentBidValue").textContent = "Nu" + bidAmount;
    document.getElementById("bidPopup").style.display = "none";
  }