
  // Edit Profile Modal
const editProfileModal = document.getElementById('edit-profile-modal');
const editProfileForm = document.getElementById('edit-profile-form');
const editNameInput = document.getElementById('edit-name');
const editEmailInput = document.getElementById('edit-email');
const editBioInput = document.getElementById('edit-bio');
const editProfileBtn = document.getElementById('edit-profile-btn');
const closeEditProfileModal = editProfileModal.querySelector('.close');

// Update Profile Picture Modal
const updateProfilePicModal = document.getElementById('update-profile-pic-modal');
const updateProfilePicForm = document.getElementById('update-profile-pic-form');
const profilePicFileInput = document.getElementById('profile-pic-file');
const updateProfilePicBtn = document.getElementById('update-profile-btn');
const closeUpdateProfilePicModal = updateProfilePicModal.querySelector('.close');

// Change Password Modal
const changePasswordModal = document.getElementById('change-password-modal');
const changePasswordForm = document.getElementById('change-password-form');
const newPasswordInput = document.getElementById('new-password');
const confirmPasswordInput = document.getElementById('confirm-password');
const changePasswordBtn = document.getElementById('change-password-btn');
const closeChangePasswordModal = changePasswordModal.querySelector('.close');

// Open Edit Profile Modal
editProfileBtn.addEventListener('click', () => {
    editProfileModal.style.display = 'block';
    editNameInput.value = document.getElementById('name').innerText;
    editEmailInput.value = document.getElementById('email').innerText;
    editBioInput.value = document.getElementById('bio').innerText;
});

// Close Edit Profile Modal
closeEditProfileModal.addEventListener('click', () => {
    editProfileModal.style.display = 'none';
});

// Open Update Profile Picture Modal
updateProfilePicBtn.addEventListener('click', () => {
    updateProfilePicModal.style.display = 'block';
});

// Close Update Profile Picture Modal
closeUpdateProfilePicModal.addEventListener('click', () => {
    updateProfilePicModal.style.display = 'none';
});

// Open Change Password Modal
changePasswordBtn.addEventListener('click', () => {
    changePasswordModal.style.display = 'block';
});

// Close Change Password Modal
closeChangePasswordModal.addEventListener('click', () => {
    changePasswordModal.style.display = 'none';
});

// Save Changes in Edit Profile Modal
editProfileForm.addEventListener('submit', (e) => {
    e.preventDefault();
    document.getElementById('name').innerText = editNameInput.value;
    document.getElementById('email').innerText = editEmailInput.value;
    document.getElementById('bio').innerText = editBioInput.value;
    editProfileModal.style.display = 'none';
});

// Upload Profile Picture
updateProfilePicForm.addEventListener('submit', (e) => {
    e.preventDefault();
    const file = profilePicFileInput.files[0];
    if (file) {
        const reader = new FileReader();
        reader.onload = function (event) {
            document.getElementById('profile-pic').src = event.target.result;
        };
        reader.readAsDataURL(file);
    }
    updateProfilePicModal.style.display = 'none';
});

// Save New Password
changePasswordForm.addEventListener('submit', (e) => {
    e.preventDefault();
    const newPassword = newPasswordInput.value;
    const confirmPassword = confirmPasswordInput.value;
    if (newPassword === confirmPassword) {
        // Save password changes here
        alert('Password changed successfully!');
    } else {
        alert('Passwords do not match. Please try again.');
    }
    changePasswordModal.style.display = 'none';
});