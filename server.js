const mongoose = require('mongoose')
const dotenv = require('dotenv')
dotenv.config({path:'./config.env'})
const app = require('./app')
const bidRoutes = require('./routes/bids');

const DB = "mongodb+srv://nexus:NTbCr8zpE8lzneA3@cluster0.mppojge.mongodb.net/cluster0?retryWrites=true&w=majority"
// const local_DB = process.env.DATABASE_LOCAL
// console.log(process.env.DATABASE_PASSWORD)

// mongoose.connect(local_DB).then((con) => {
//     console.log(con.conections)
//     console.log('DB connection succesful')
// }).catch(error => console.log(error));

mongoose.connect(DB).then((con) => {
    console.log(con.connections)
    console.log('DB connection succesful')
}).catch(error => {
    console.log(error);
})

/* Starting the port on port 4001. */
const port = 4001
app.listen(port, ()=>{
    console.log(`App running on port ${port} ..`)
})

// Middleware
// app.use(express.json());
// Routes
app.use('/bids', bidRoutes);