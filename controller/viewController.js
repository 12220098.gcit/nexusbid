const path = require('path')

/* LOG IN PAGE */
exports.getLoginForm = (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'views', 'login.html'))
}

/* SIGN UP PAGE */
exports.getSignupForm = (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'views', '/', 'signup', 'signup.html'))
}

/* HOME PAGE */
exports.getHome = (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'views', 'index.html'))
}

/* Profile Page */
exports.getProfile = (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'views', 'userProfile.html'))
}

exports.getHome2 = (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'views', 'home.html'))
}

exports.getAdmin = (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'views', '/admindashboard', 'Dashboard.html'))
}

exports.getProducts = (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'views', 'product.html'))
}

exports.getAdd_Products = (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'views', '/admindashboard', 'addproduct.html'))
}