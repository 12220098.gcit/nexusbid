const Bid = require('../models/Bid');

// Create a new bid
exports.createBid = async (req, res) => {
  try {
    const { productId, amount, bidder } = req.body;

    const bid = new Bid({
      productId,
      amount,
      bidder,
    });

    await bid.save();

    res.status(201).json(bid);
  } catch (err) {
    res.status(500).json({ error: 'Failed to create a bid' });
  }
};

// Get all bids for a product
exports.getBidsByProduct = async (req, res) => {
  try {
    const { productId } = req.params;

    const bids = await Bid.find({ productId });

    res.json(bids);
  } catch (err) {
    res.status(500).json({ error: 'Failed to get bids' });
  }
};
